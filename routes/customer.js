const express = require("express");
const fs = require("fs");
const path = require("path");
const multer = require("multer");
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "public/assets/images");
  },
  filename: (req, file, cb) => {
    cb(
      null,
      file.fieldname + "-" + Math.floor(Math.random() * 9999999) + ".jpg"
    );
  },
});
var upload = multer({ storage: storage });

const router = express.Router();
const { validate, Customer } = require("../models/customer");
var sorting = false;
var customers;

router.get("/", async (req, res) => {
  var customers = await Customer.find().sort("name");
  res.render("index", { customers: customers });
});

router.get("/cards", async (req, res) => {
  var customers = await Customer.find().sort("name");
  res.render("index-cards", { customers: customers });
});

router.get("/name", async (req, res) => {
  if (sorting) {
    var customers = await Customer.find().sort("name");
    customers.reverse();
  } else {
    var customers = await Customer.find().sort("name");
  }
  sorting = !sorting;
  res.render("index", { customers: customers });
});
router.get("/age", async (req, res) => {
  if (sorting) {
    var customers = await Customer.find().sort("age");
    customers.reverse();
  } else {
    var customers = await Customer.find().sort("age");
  }
  sorting = !sorting;
  res.render("index", { customers: customers });
});
router.get("/mobile", async (req, res) => {
  if (sorting) {
    var customers = await Customer.find().sort("mobile");
    customers.reverse();
  } else {
    var customers = await Customer.find().sort("mobile");
  }
  sorting = !sorting;
  res.render("index", { customers: customers });
});
router.get("/isGold", async (req, res) => {
  if (sorting) {
    var customers = await Customer.find().sort("isGold");
    customers.reverse();
  } else {
    var customers = await Customer.find().sort("isGold");
  }
  sorting = !sorting;
  res.render("index", { customers: customers });
});
router.get("/add", (req, res) => {
  res.render("add-customer", { errorInput: undefined });
});
router.get("/show/:id", async (req, res) => {
  const customer = await Customer.findById(req.params.id);
  if (!customer)
    return res
      .status(404)
      .send("The Customer with the given ID was not found.");
  res.render("show-customer", {
    customer: customer,
    errorInput: "",
  });
});

router.get("/delete/:id", async (req, res) => {
  let customer = await Customer.findByIdAndUpdate(req.params.id, {
    visible: false,
  });
  if (!customer)
    return res
      .status(404)
      .send("The Customer with the given ID was not found.");
  res.redirect("/api/customer");
});

router.get("/deletecards/:id", async (req, res) => {
  let customer = await Customer.findByIdAndUpdate(req.params.id, {
    visible: false,
  });
  if (!customer)
    return res
      .status(404)
      .send("The Customer with the given ID was not found.");
  res.redirect("/api/customer/cards");
});

router.post(
  "/customers/:id",
  upload.single("avatar"),
  async (req, res, next) => {
    let customer1 = await Customer.findById(req.params.id);
    const { error } = validate(req.body);
    //validate input
    // if (error)
    //   return res.status(400).render("show-customer", {
    //     customer: customer1,
    //     errorInput: error.details[0].message,
    //   });
    //.render("/customers/:id", { errorInput: error.details[0].message });

    console.log(req.body);

    var customerImage = customer1.image;

    if (req.file) {
      customerImage = {
        data: fs.readFileSync(
          path.join(__dirname, "/../public/assets/images", req.file.filename)
        ),
        contentType: "image/jpeg",
      };
    }

    let customer2 = await Customer.findByIdAndUpdate(req.params.id, {
      name: req.body.name,
      mobile: req.body.mobile,
      isGold: req.body.isGold,
      isFemale: req.body.isFemale,
      age: req.body.age,
      image: customerImage,
    });
    //if (!customer) return res.status(404).send("The Customer with the given ID was not found.");
    if (!customer2)
      return res.status(404).render("show-customer", {
        customer: customer1,
        errorInput: "The Customer with the given ID was not found.",
      });
    res.redirect("/api/customer");
  }
);


router.put(
  "/customers/:id",
  upload.single("avatar"),
  async (req, res, next) => {
    let customer1 = await Customer.findById(req.params.id);
    const { error } = validate(req.body);
    //validate input
    // if (error)
    //   return res.status(400).render("show-customer", {
    //     customer: customer1,
    //     errorInput: error.details[0].message,
    //   });
    //.render("/customers/:id", { errorInput: error.details[0].message });

    console.log(req.body);

    var customerImage = customer1.image;

    if (req.file.filename) {
      customerImage = {
        data: fs.readFileSync(path.join(
        __dirname,
        "/../public/assets/images",
        req.file.filename
      )),
        contentType: "image/jpeg",
      }
    }

    let customer2 = await Customer.findByIdAndUpdate(req.params.id, {
      name: req.body.name,
      mobile: req.body.mobile,
      isGold: req.body.isGold,
      isFemale: req.body.isFemale,
      age: req.body.age,
      image: customerImage,
    });
    //if (!customer) return res.status(404).send("The Customer with the given ID was not found.");
    if (!customer2)
      return res.status(404).render("show-customer", {
        customer: customer1,
        errorInput: "The Customer with the given ID was not found.",
      });
    res.redirect("/api/customer");
  }
);

router.post("/", upload.single("avatar"), async (req, res, next) => {
  const { error } = validate(req.body);
  //schema.validate({ a: "a string" });
  //if (error) return res.status(400).send(error.details[0].message);
  if (error)
    return res
      .status(400)
      .render("add-customer", { errorInput: error.details[0].message });

  var customerImageFilename = path.join(
    __dirname,
    "/../public/assets/photo_placeholder.jpg"
  );

  if (req.file) {
    customerImageFilename = path.join(
      __dirname,
      "/../public/assets/images",
      req.file.filename
    );
  }

  let customer = new Customer({
    name: req.body.name,
    mobile: req.body.mobile,
    isGold: req.body.isGold,
    isFemale: req.body.isFemale,
    age: req.body.age,
    image: {
      data: fs.readFileSync(customerImageFilename),
      contentType: "image/jpeg",
    },
  });

  customer = await customer.save();
  res.redirect("/api/customer");
});

router.delete("/customer/:id", async (req, res) => {
  let customer1 = await Customer.findById(req.params.id);
  const { error } = validate(req.body);
  //validate input
  if (error)
    return res.status(400).render("show-customer", {
      customer: customer1,
      errorInput: error.details[0].message,
    });
  //.render("/customers/:id", { errorInput: error.details[0].message });

  console.log(req.body);
  let customer2 = await Customer.findByIdAndDelete(req.params.id);
  //if (!customer) return res.status(404).send("The Customer with the given ID was not found.");
  if (!customer2)
    return res.status(404).render("show-customer", {
      customer: customer1,
      errorInput: "The Customer with the given ID was not found.",
    });
  res.redirect("/api/customer");
});

module.exports = router;
