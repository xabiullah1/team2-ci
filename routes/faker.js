const express = require("express");
const router = express.Router();
const faker = require("faker");
const { Customer } = require("../models/customer");

const http = require("http");
const fs = require("fs");
const { contentType } = require("express/lib/response");
const path = require("path");

const NUMBER_OF_FAKE_CUSTOMERS = 50;

let randomImages = fs.readdirSync(path.join(__dirname, "../public/assets/images"));

router.get("/", (req, res) => {
  for (let index = 0; index < NUMBER_OF_FAKE_CUSTOMERS; index++) {
    var customer = new Customer();
    customer.isFemale = faker.random.boolean();
    customer.name = faker.name.findName(
      undefined,
      undefined,
      customer.isFemale ? "1" : "0"
    );
    customer.mobile = faker.phone.phoneNumber("+9665########");
    customer.isGold = faker.random.boolean();
    customer.age = faker.random.number({ min: 18, max: 70 });
    customer.visible = true;
    var imageFilename = randomImages[index];
    customer.image = {
      data: fs.readFileSync(
        path.join(__dirname, "../public/assets/images", imageFilename)
      ),
      contentType: "image/jpeg",
    };

    customer.save((err) => {
      if (err) throw err;
    });

    // customer.imageUrl = faker.image.people(312, 312, true);
  }
  res.redirect("/api/customer");
});

module.exports = router;