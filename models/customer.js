const Joi = require("joi");
const mongoose = require("mongoose");

const Customer = mongoose.model(
  "Customer",
  new mongoose.Schema({
    name: {
      type: String,
      required: true,
      minlength: 3,
      maxlength: 255,
    },
    mobile: {
      type: String,
      required: true,
      minlength: 3,
      maxlength: 100,
    },
    isGold: {
      type: Boolean,
      default: false,
    },
    age: {
      type: Number,
      required: true,
    },
    visible: {
      type: Boolean,
      default: true,
    },
    // imageUrl: {
    //   type: String,
    //   default: "/public/assets/photo_placeholder.jpg",
    // },
    image: {
      data: Buffer,
      contentType: String
    },
    isFemale: {
      type: Boolean,
      default: false,
    },
  })
);

function validateCustomer(customer) {
  const pattern = "^[+]?[(]?[0-9]{3}[)]?[-s.]?[0-9]{3}[-s.]?[0-9]{4,6}$";
  customer.age = parseInt(customer.age);
  
  if (customer.isGold) {
    customer.isGold = true;
  } else {
    customer.isGold = false;
  }
  
  console.log('BEFORE: ' + customer.isFemale);
  
  if (customer.isFemale == '1') {
    customer.isFemale = true;
  } else {
    customer.isFemale = false;
  }

  console.log("AFTER: " + customer.isFemale);
  
  const schema = Joi.object({
    name: Joi.string().min(3).max(255).required(),
    mobile: Joi.string().pattern(new RegExp(pattern)).required(),
    isGold: Joi.boolean(),
    isFemale: Joi.boolean(),
    age: Joi.number().required().min(18).max(99),
    visible: Joi.boolean(),
    // imageUrl: Joi.string().uri()
  });
  return schema.validate(customer);
}

exports.Customer = Customer;
exports.validate = validateCustomer;
