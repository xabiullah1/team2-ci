const mongoose = require('mongoose');
const express = require('express');
const app = express();
const customers = require('./routes/customer');
const search = require('./routes/search');
const searchcards = require("./routes/searchcards");
const fakeData = require('./routes/faker');
var methodOverride = require('method-override');
// 1 : Connect to db
mongoose
  .connect("mongodb+srv://admin:admin1234@cluster0.6rcgn.mongodb.net/NITA")
  .then(() => console.log("Connected to DB Server ..."))
  .catch((err) => console.log("Connection to DB Server Failed"));

// Make Mongoose use `findOneAndUpdate()`. Note that this option is `true`
// by default, you need to set it to false.
mongoose.set('useFindAndModify', false);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(
  methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
      // look in urlencoded POST bodies and delete it
      var method = req.body._method;
      delete req.body._method;
      return method;
    }
  })
);
app.set('view engine', 'ejs');
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/public', express.static(__dirname + '/public'));
app.use('/api/customer', customers);
app.use('/api/fake', fakeData);
app.use('/api/search', search);
app.use("/api/searchcards", searchcards);
const port = process.env.PORT || 3000;

app.get('/', (req, res) => {
  res.redirect('/api/customer');
});

app.use("/icons", express.static(__dirname + "/node_modules/bootstrap-icons/icons"));

app.get("/team", (req, res) => {
  res.render("team");
});

app.listen(port, () => console.log(`Listening on port ${port}`));
